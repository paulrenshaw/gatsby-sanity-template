module.exports = async (graphql, collection, limit) => {
  return await graphql(
    `
      query CollectionListQuery($collection: String!, $limit: Int) {
        allSanityPage(
          filter: { collection: { id: { eq: $collection } } }
          limit: $limit
        ) {
          edges {
            node {
              _type
              id
              title
              description
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              image {
                asset {
                  fluid {
                    base64
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                  }
                }
              }
            }
          }
        }
      }
    `,
    { collection: collection.id, limit: limit || 10 }
  )
}
