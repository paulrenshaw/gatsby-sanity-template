module.exports = async (graphql, collection, category, featured, limit) => {
  return await graphql(
    `
      query CollectionListQuery(
        $category: String
        $collection: String!
        $featured: Boolean
        $limit: Int
      ) {
        allSanityPage(
          filter: {
            featured: { eq: $featured }
            collection: { id: { eq: $collection } }
            category: { id: { eq: $category } }
          }
          limit: $limit
        ) {
          edges {
            node {
              _type
              id
              title
              description
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              image {
                asset {
                  fluid {
                    base64
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                  }
                }
              }
            }
          }
        }
      }
    `,
    {
      collection: collection.id,
      category: category ? category.id : null,
      featured,
      limit: limit || 10,
    }
  )
}
