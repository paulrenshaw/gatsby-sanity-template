module.exports = async (graphql, collection, category, limit) => {
  return await graphql(
    `
      query CollectionListQuery(
        $category: String
        $collection: String!
        $limit: Int
      ) {
        allSanityPage(
          filter: {
            collection: { id: { eq: $collection } }
            category: { id: { eq: $category } }
          }
          limit: $limit
        ) {
          edges {
            node {
              _type
              id
              title
              description
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              image {
                asset {
                  fluid {
                    base64
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                  }
                }
              }
            }
          }
        }
      }
    `,
    { collection: collection.id, category: category.id, limit: limit || 10 }
  )
}
