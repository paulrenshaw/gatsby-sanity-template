module.exports = async (graphql, collection, featured, limit) => {
  return await graphql(
    `
      query CollectionListQuery(
        $collection: String!
        $featured: Boolean
        $limit: Int
      ) {
        allSanityPage(
          filter: {
            featured: { eq: $featured }
            collection: { id: { eq: $collection } }
          }
          limit: $limit
        ) {
          edges {
            node {
              _type
              id
              title
              description
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              image {
                asset {
                  fluid {
                    base64
                    aspectRatio
                    src
                    srcSet
                    srcWebp
                    srcSetWebp
                    sizes
                  }
                }
              }
            }
          }
        }
      }
    `,
    { collection: collection.id, featured, limit: limit || 10 }
  )
}
