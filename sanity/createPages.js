const featuredCategoryPages = require('./collectionPageQueries/featuredCategoryPages')
const categoryPages = require('./collectionPageQueries/categoryPages')
const featuredPages = require('./collectionPageQueries/featuredPages')
const allPages = require('./collectionPageQueries/allPages')

module.exports = async (graphql, createPage) => {
    const result = await graphql(`
      query PagesQuery {
        allSanityPage {
          edges {
            node {
              _id
              id
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              sections {
                _key
                list {
                  _key
                  list {
                    ... on SanityCollectionList {
                      _type
                      id
                      featured
                      limit
                      category {
                        id
                      }
                      collection {
                        id
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `)

  if (result.errors) {
    throw result.errors
  }

  const pages = result.data.allSanityPage.edges || []
  pages.forEach(async (edge, index) => {
    
    const { _id, id, collection, sections, slug } = edge.node
    let path = ''
    
    if (!slug && (_id === "index" || _id === "drafts.index")) path = '/'
    else if (slug && collection && collection.slug) path = `/${collection.slug.current}/${slug.current}`
    else if (slug && !collection) path = `/${slug.current}`
    else return null

    let collectionLists = []

    const promises = sections.map(async section => {
      const { list } = section
      if (list && list.list) {
        const _list = list.list
        if (_list._type === 'collectionList' && _list.collection) {
          const { category, collection, featured, limit } = _list
          let collectionItems
          
          if (category && featured) {
            collectionItems = await featuredCategoryPages(graphql, collection, category, featured, limit)
          } else if (category) {
            collectionItems = await categoryPages(graphql, collection, category, limit)
          } else if (featured) {
            collectionItems = await featuredPages(graphql, collection, featured, limit)
          } else {
            collectionItems = await allPages(graphql, collection, limit)
          }

          if (collectionItems.errors) {
            throw collectionItems.errors
          }

          collectionLists.push({
            sectionKey: section._key,
            items: collectionItems.data.allSanityPage.edges.map(edge => edge.node)
          })

        }
      }

      return
    })

    await Promise.all(promises)

    createPage({
      path,
      component: require.resolve("../src/templates/SanityPage.tsx"),
      context: { id, collectionLists },
    })
  })
}