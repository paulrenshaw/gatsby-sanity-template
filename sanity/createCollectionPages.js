module.exports = async (graphql, createPage) => {
    const result = await graphql(`
    {
      allSanityPage(filter:{slug:{current:{ne:null}}, collection:{id:{ne:null}}}) {
        edges {
          node {
            id
            slug {
              current
            }
            collection {
              id
              slug {
                current
              }
            }
          }
        }
      }
    }
  `)

  if (result.errors) {
    throw result.errors
  }

  const pages = result.data.allSanityPage.edges || []
  pages.forEach((edge, index) => {
    const { id, slug, collection } = edge.node
    const path = `/${collection.slug.current}/${slug.current}`
    createPage({
      path,
      component: require.resolve("../src/templates/SanityPage.tsx"),
      context: { id, slug: slug.current },
    })
  })
}