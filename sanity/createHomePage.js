module.exports = async (graphql, createPage) => {
  const result = await graphql(`
    {
      sanityPage(_id: { eq: "index" }) {
        id
      }
    }
  `)

  if (result.errors) {
    throw result.errors
  }

  createPage({
    path: "/",
    component: require.resolve("../src/templates/SanityPage.tsx"),
    context: { id: result.data.sanityPage.id },
  })
}
