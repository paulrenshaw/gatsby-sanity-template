/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

// You can delete this file if you're not using it

// const createSanityCollectionPages = require('./sanity/createCollectionPages')
// const createSanityHomePage = require('./sanity/createHomePage')
const createSanityPages = require('./sanity/createPages')

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  // await createSanityHomePage(graphql, createPage)
  await createSanityPages(graphql, createPage)
  // await createSanityCollectionPages(graphql, createPage)
}
