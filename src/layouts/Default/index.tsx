/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"

import Navbar from "../../components/Navbar"
import "./default.css"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
  query NavigationQuery {
    site: site {
      siteMetadata {
        title
      }
    }
    navigation: sanitySite(_id: {eq: "site"}) {
      menu {
        items {
          title
          default {
            ... on SanityLink {
              _key
              _type
              path
              title
            }
            ... on SanityPage {
              id
              collection {
                slug {
                  current
                }
              }
              slug {
                current
              }
              title
              _type
              _id
            }
          }
          menu {
            ... on SanityLink {
              _key
              _type
              path
              title
            }
            ... on SanityPage {
              id
              _key
              title
              slug {
                current
              }
              collection {
                slug {
                  current
                }
              }
              _type
              _id
            }
          }
        }
      }
    }
  }
  `)

  const links = data.navigation.menu.items.map(item => {
    if (item.default[0].collection) {
      return {
        to: `/${item.default[0].collection.slug.current}/${item.default[0].slug.current}`,
        title: item.title
      }
    } else {
      return {
        to: `/${item.default[0].slug.current}`,
        title: `${item.title}`
      }
    }
  })

  return (
    <>
      <Navbar siteTitle={data.site.siteMetadata.title} links={links} />
      <main>{children}</main>
      <footer>
        © {new Date().getFullYear()}, Built with
        {` `}
        <a href="https://www.gatsbyjs.org">Gatsby</a>
        {` and `}
        <a href="https://sanity.io">Sanity</a>
      </footer>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
