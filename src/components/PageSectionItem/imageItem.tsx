import React from 'react'
import Img from 'gatsby-image'

const ImageItem = ({ image }) => {
    return (
        <div>
            <Img fluid={image.fluid} />
        </div>
    )
}

export default ImageItem