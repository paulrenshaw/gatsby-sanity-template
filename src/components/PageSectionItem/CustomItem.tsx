import React from "react"
import BlockContent from "@sanity/block-content-to-react"
import Img from "gatsby-image"

function CustomItem({ item }) {
  const { _rawContent, background, cta, image, layout } = item
  return (
    <div style={{ textAlign: layout?.textAlign ? layout.textAlign : "left" }}>
      {image && image.asset ? (
        !layout ||
        !layout?.imagePosition ||
        layout?.imagePosition === "top" ||
        layout?.imagePosition === "left" ? (
          <Img fluid={image.asset.fluid} />
        ) : null
      ) : null}

            
      {_rawContent ? 
        <div style={{ padding: '1em' }}>
            <BlockContent blocks={_rawContent} />
        </div>
      : null}

      {image && image.asset ? (
        layout?.imagePosition === "right" ||
        layout?.imagePosition === "bottom" ? (
          <Img fluid={image.asset.fluid} />
        ) : null
      ) : null }
    </div>
  )
}

export default CustomItem
