import React from 'react'
import CustomItem from './CustomItem'
import PageItem from './PageItem'

const PageSectionItem = ({ item }) => {
    console.log({item})
    const { _type } = item
    return (
        <div>
            { _type === "customItem" ? <CustomItem item={item} /> : null }
            { _type === "page" ? <PageItem page={item} /> : null }
        </div>
    )
}

export default PageSectionItem