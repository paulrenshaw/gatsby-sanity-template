import React from 'react'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

const PageItem = ({ page }) => {
    const { title, description, image, collection, slug } = page
    console.log(collection, slug)
        const link = collection 
            ? `/${collection.slug.current}/${slug.current}`
            : `/${slug.current}`
    return (
        <div>
            <Link to={link} style={{padding: '1em'}}>
                { title ? <h3>{ title }</h3> : null }
                { image && image.asset ? <Img fluid={image.asset.fluid} /> : null }
                { description ? <p>{ description }</p> : null }
            </Link>
        </div>
    )
}

export default PageItem