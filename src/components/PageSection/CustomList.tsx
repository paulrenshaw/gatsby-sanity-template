import React from 'react'
import CustomItem from '../PageSectionItem/CustomItem'
import PageItem from '../PageSectionItem/PageItem'
import Img from 'gatsby-image'

const listStyle = () => {
  return {
    display: 'flex',
    flexWrap: 'wrap'
  }
}

const itemStyle = (listLayout, itemLayout, background) => {
  const { fluidItems, itemsPerRow } = listLayout
  const { style, textAlign } = itemLayout
  const { blend, clip, colors, contain, fixed, gradient } = background

  const padding = !style || style === 'plain' ? '1em' : '0'
  
  return {
    position: 'relative',
    minWidth: 100 / ( itemsPerRow || 1 ) + '%',
    maxWidth: fluidItems ? '100%' : 100 / ( itemsPerRow || 1 ),
    flex: fluidItems ? '1' : '0',
    textAlign,
    background: colors?.length ? colors[0].color.hex : 'inherit',
    backgroundClip: clip ? 'content-box' : 'border-box'
  }
}

function CustomItemList ({items, layout}) {
  return items.map(item => {
   return ( 
    <div style={itemStyle(layout || {}, item.layout || {}, item.background || {})} key={item._key || item.id} >
      { item._type === 'customItem' ? <CustomItem item={item} /> : null }
      { item._type === 'page' ? <PageItem page={item} /> : null }
    </div>
   )
  })
}

function CustomList ({ list, layout }) {
  return (
    <div style={listStyle()}>
      { list.items ? <CustomItemList items={list.items} layout={layout} /> : null }
    </div>
  )
}

export default CustomList