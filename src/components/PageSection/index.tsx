import React from "react"
import BlockContent from "@sanity/block-content-to-react"

import CustomList from "./CustomList"
import CollectionList from "./CollectionList"

import { containerWidth } from "../../styleVariables"

const sectionStyle = layout => {
  const { fullWidth } = layout
  return {
    maxWidth: fullWidth ? "100%" : containerWidth,
    margin: "auto",
  }
}

const PageSectionList = ({ list }) => {
  if (list.list._type === "collectionList")
    return <CustomList list={list.list} layout={list.layout} key={list._key} />
  if (list.list._type === "customList")
    return <CustomList list={list.list} layout={list.layout} key={list._key} />
  return null
}

function PageSection({ section }) {
  return (
    <section style={sectionStyle(section.layout || {})}>
      {section.heading || section.content ? (
        <div
          style={{
            maxWidth: containerWidth || "960px",
            margin: "auto",
            padding: "1em",
          }}
        >
          {section.isH1 ? (
            <h1>{section.heading}</h1>
          ) : (
            <h2>{section.heading}</h2>
          )}

          {section.content?.content ? (
            <BlockContent blocks={section.content.content} />
          ) : null}
        </div>
      ) : null}

      { section.list ? <PageSectionList list={section.list} /> : null }
    </section>
  )
}

export default PageSection
