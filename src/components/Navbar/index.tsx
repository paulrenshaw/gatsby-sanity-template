import PropTypes from "prop-types"
import React from "react"

import NavLink from './NavLink'

const navbarStyle = {
  background: `rebeccapurple`,
  marginBottom: `1.45rem`,
}

const navStyle = {
  margin: `0 auto`,
  maxWidth: 960,
  padding: `1.45rem 1.0875rem`,
  display: 'inline-block',
}

const Navbar = ({ siteTitle, links }) => {

  return (
    <div style={navbarStyle}>
      <nav style={navStyle} >
        <NavLink to="/" title={siteTitle} isSiteTitle={true} />
        { links.map(({title, to}) => <NavLink title={title} to={to} key={to} />) }
      </nav>
    </div>
    
  )

}

Navbar.propTypes = {
  siteTitle: PropTypes.string,
  links: PropTypes.arrayOf(PropTypes.shape({
      to: PropTypes.string,
      title: PropTypes.string
    }
  ))
}

Navbar.defaultProps = {
  siteTitle: '',
  links: []
}

export default Navbar
