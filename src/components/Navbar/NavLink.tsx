import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const navLinkStyle = {
  color: `white`,
  textDecoration: `none`,
  marginRight: `2em`
}

const siteTitleStyle = {
  color: `white`,
  fontWeight: 700,
  fontSize: `1.5rem`,
  marginLeft: `1em`
}

const NavLink = ({ to, title, isSiteTitle }) => {
  return (
    <Link to={to} style={navLinkStyle} key={to} >
      { isSiteTitle ? <span style={siteTitleStyle}>{title}</span> : title }
    </Link>
  )
}

NavLink.propTypes = {
  to: PropTypes.string,
  title: PropTypes.string,
  isSiteTitle: PropTypes.bool
}

export default NavLink