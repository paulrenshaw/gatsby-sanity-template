import React from "react"

import PageSection from "../components/PageSection"
import Layout from "../layouts/Default"
import SEO from "../components/SEO"
import { graphql } from "gatsby"

export const query = graphql`
  query($id: String!) {
    page: sanityPage(id: { eq: $id }) {
        title
        slug {
          current
        }
        sections {
          list {
            list {
              ... on SanityCustomList {
                id
                items {
                  ... on SanityCustomItem {
                      _rawContent
                    background {
                      blend
                      clip
                      contain
                      fixed
                      gradient
                      colors {
                        color {
                          hex
                        }
                      }
                      image {
                          asset {
                              fluid {
                                base64
                                aspectRatio
                                src
                                srcSet
                                srcWebp
                                srcSetWebp
                                sizes
                              }
                          }
                      }
                    }
                    cta {
                      color {
                        color {
                          hex
                        }
                      }
                      link {
                        path
                        title
                      }
                    }
                    image {
                      asset {
                        fluid {
                          base64
                          aspectRatio
                          src
                          srcSet
                          srcWebp
                          srcSetWebp
                          sizes
                        }
                      }
                    }
                    layout {
                      imagePosition
                      style
                      textAlign
                    }
                    _type
                    _key
                  }
                }
                _type
              }
              ... on SanityCollectionList {
                _type
                id
                featured
                limit
                category {
                  id
                }
                collection {
                  id
                }
              }
            }
            layout {
              fluidItems
              itemsPerRow
            }
          }
          _rawContent
          background {
            blend
            clip
            contain
            fixed
            gradient
            colors {
              color {
                hex
              }
            }
            image {
              asset {
                fluid {
                  base64
                  aspectRatio
                  src
                  srcSet
                  srcWebp
                  srcSetWebp
                  sizes
                }
              }
            }
          }
          color {
            color {
              hex
            }
          }
          heading
          isH1
          layout {
            fullWidth
          }
          _key
        }
        background {
          blend
          clip
          colors {
            color {
              hex
            }
          }
          contain
          fixed
          gradient
          image {
            asset {
              fluid {
                base64
                aspectRatio
                src
                srcSet
                srcWebp
                srcSetWebp
                sizes
              }
            }
          }
        }
        collection {
          slug {
            current
          }
          title
        }
        description
        image {
          asset {
            fluid {
              base64
              aspectRatio
              src
              srcSet
              srcWebp
              srcSetWebp
              sizes
            }
            source {
              url
            }
          }
        }
      }
  }
`

const Page = ({ children, data, pageContext }) => {
  const page = data.page
  const { collectionLists } = pageContext

  return (
    <Layout>
      <SEO title={page.title} description={page.description} />
      {
        page.sections.map(section => {
            if (section.list && section.list.list._type === 'collectionList') {
                section.list.list.items = collectionLists.find(list => list.sectionKey === section._key).items
            }
            return <PageSection 
                    section={section} 
                    key={section._key} />
        })
      }
      {children}
    </Layout>
  )
}

export default Page
